# 1. Gestion de projet et documentation



## 1.1 Introduction: Par ou commencer?  

Tout d'abord, après avoir suivis le 1er cours nous expliquant dans les grandes lignes le but de toute ses installations, me sentant complètement sorti de "*ma zone de confort*", je demande a Quentin de m'expliquer **pourquoi gitlab?**. 



### 1.1.1 L'intérêt de gitlab 

C'est un **logiciel libre** basé sur git qui a pour but le partage de données (pour des projets) de manière sécurisée. Sa force étant **la mémorisation de l'historique des modifications** grâce à des balises. 



## 1.2 CLI (Command-Line Interface)
### 1.2.1 Qu'est ce que c'est? 
C'est une interface textuelle traitant des commandes vers un programme informatique via un **Shell Bash**. 
Contrairement au GUI (interface graphique utilisateur) qui a recours à des éléments graphiques pour faciliter l'utilisation. 

### 1.2.2 Git Bash 
La suite de tutoriels du cours me conseillait d'installer un certain Bash mais le but étant gitlab des amis informaticiens m'ont conseillé d'**installer git Bash** car cela me permettrait d'avoir git et un shell. 
Par la suite j'ai remarqué que qu'il existait déjà **un shell sur Windows 11** du coup j'ai utilisé les 2 en parallèle. 

![git_bash](../images/git_bash.png)
![bash_windows](../images/bash_windows.png)
 

## 1.3 Comprendre LINUX
Je ne comprenais pas pourquoi cette étape au départ. Etape importante car tous les outils utilisés (git) sont natifs de Linux 

### 1.3.1 Linux 
C'est un système d'exploitation en Open source.  

### 1.3.2 Utilisation de Linux 
J'ai téléchargé une machine vituelle : **VirtuelBox**. J'ai ensuite configuré cette machine avec **Ubuntu**(qui est une distribution friendly de Linux) MAIS je ne su l'exploité car je n'arrivais pas à avoir accès à internet dessus.
Le dérangement fu de courte durée car j'ai remarqué que je n'étais pas obligé de passer par Linux pour avoir accès au shell.  


#### 1.3.3 Table de commande Bash 

| Commande |  Description    |  Option  |           Description         |
|-----|-----------------|---------|--------------------------|
| `pwd`  | Indique le répertoire    |  |   |
| `cd`  |Revenir au dossier parent  |  `cd ..` |revenir au dossier précédent
|||`cd../..`| revenir au dossier précédent précédent | 
|||`cd ~/Desktop`| a partir de mon répertoire perso       |
| `ls`   | liste de tout les dossiers  |  `ls -a`| voir les documents cachés   |        |
| `mkdir`  | créer un dossier  | `mkdir -p` | créer un dossier ds d'autres dossier   |        |
| `mv`  | déplacer un dossier dans un autre répertoire   |  `mv "doc1" "doca"`| renommer dossier   |        |
| `rmdir`  | supprimer un dossier |`rmdir -p`|supprimer aussi répertoire parent   |        |
| `cp`  | copier un dossier |  |    |        |
|`cat`| voir le texte d'un doc||||
|`wc -l`| donner le nbre de ligne d'un texte||||

Remarque : les commandes de **copier/coller** ne fonctionnait que sur 1 bash sur 2 c'est pourquoi le prof m'a conseillé de travailler que sur Git Bash. 
**Sudo** fonctionnait sur aucun des 2. 

## 1.4 Markdown 
### 1.4.1 Définition 
C'est le **language de balisage léger** qu'on utilisera pour écrire notre site. Il est intéressant car il offre **une syntaxe facile d'utilisation**.  

### 1.4.2 Tableau de commande 
On peut retrouver la liste des commandes sur [Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/). 

### 1.4.3 Visual studio (VS code)
Je vais utiliser cet éditeur de texte pour écrire ma documentation. Il est intéressant car il utilise le language **Markdown**. 
Voici un exemple de l'interface de celui-ci :

![Photo_markdown](../images/markdown_exemple.png)

## 1.5 Git 
### 1.5.1 Pourquoi Git?  
Permet de travailler sur un projet en local. Donc avoir une copie du projet sur notre pc. IL est également intéressant car permet de travaillera plusieurs sur le même projet. 


### 1.5.2 Commandes git 
| Commande | Description |
|-------|-----------------|
| `git clone adresseGitRepo`| Faire une copie du -repository- sur le pc|
 |`git add fileName`|  ajouter les modifications|
 |`git commit -m "message"`| dire les modifications avec un message| 
 |`git push`| envoyer les changements sur le serveur|
 |`git pull`|mettre notre branche locale avec le changement sur le serveur|




### 1.5.3 Protocole SSH
Tout d'abord je vais configurer git pour que mon username et mon email correspondent à ceux de GitLab. 

![config](../images/microcontroleur/mkdown_exemple.png) 

Maintenant je vais créer une paire de clés avec "ED25519". A la fin du processus j'aurai une clé publique et une clef personnel qui sont des mots de passe. 

![clefs_SSH](../images/clef_SSH.png)

Ensuite j'encode ma clef publique sur Gitlab. 
![clef_publique](../images/clef_publique%20.png)
![clef_sur_gitlab](../images/clef_SSH_gitlab.png)

Malheureusement quand j'ai donné la clef publique à gitlab, mon pc n'a pas été reconnu. C'est pourquoi vu mon énorme retard **j'ai suivi le conseil de Stanislas**. Opter pour un protocole alternatif : **le protocole HTTPS** (qui est intéressant car déjà configuré). 

### 1.5.4 Cloner mon *repertory*
je vais copier le dossier du serveur via la fonction `git clone` et confirmer la présence de la copie avec `ls`.
![git_clone](../images/git_clone.png) 
![git_clone2](../images/git_clone2.png)

## 1.6 MkDocs
Il est utile de l'installer pour pouvoir **convertir son texte markdown en HTML** et ainsi le voir en local (pour voir un aperçu).        
`python -m mkdocs serve`

## 1.7. Images 
Pour ajouter des photos a aux projets il faudra au préalable les travailler en les compressant pour les rendre moins lourd. Pour ce faire le programme choisi sera **GIMP**. Sa compréhension se fera en allant sur [Gimp Quickies](https://www.gimp.org/tutorials/GIMP_Quickies/#changing-the-size-filesize-of-a-jpeg) 

![Gimp](../images/gimp_tuto.png)

## 1.7 Avantages de VScode
Les commandes comme `git add`, `git commit`et `git push`peuvent être activer via le GUI de VScode (pour plus de facilité). 
