# 2. Conception Assistée par Ordinateur (CAO)



## 2.1. Introduction
Le but de ce module est la modélisation d'un kit de construction paramétrique FlexLinks qu'on imprimera avec l'imprimante 3D dans le prochain module. 

## 2.2. Logiciel 
- `Inkscape` : Logiciel de conception 2D via des dessins vectoriels.   
- `FreeCAD` : Logicel de modélisation paramétrique volumique, surfacique et fonctionnelle.
- `OpenSCAD` : Logiciel de CAD paramétrique basé sur la géométrie pratique pour la 3 et 2D. 

L'avantage de ces logiciels c'est qu'ils sont gratuit et en Open source. 

## 2.3 Type d'image 
### 2.3.1 Image matriciel 
Ce sont des **images composées de pixels**(des points individuel formant l'image). Chaque pixel déterminera la couleur et la luminosité de l'image. Le **redimensionnement** de ces pixels peut entrainer une **perte de qualité** car on peut juste jouer sur leur taille. Ce sont des document **jpg, png et xcf** 
### 2.3.2 Image vectoriel 
Elles sont composées de **formes géométriques** (lignes, courbes et polynômes). Ce sont donc tous ces vecteurs qui vont déterminer la position, la forme et la taille de chaque élément composant l'image. 
Elles sont donc plus intéressante que les images matriciels pour le redimensionnement.
## 2.4 Modélisation 2D
Pour ce faire on utilisera **Inkscape** qui sera utile pour la conception de logos, d'icone, de diagramme et de schémas techniques. Pour comprendre son fonctionnement consulte la page : [les BASES d'Inkscape en 20min](https://www.youtube.com/watch?v=GSGaY0-4iik) 

![Inkscape](../images/Inkscape.png)

Remarque : je n'ai pas vraiment eu le temps de chipoter dessus car j'ai beaucoup de retard. 
## 2.5 Modélisation 3D 
Il est important de noter que l'imprimante 3D ne peut pas lire des images matricielles c'est pourquoi il faut travailler avec des images vectorielles.    
J'avais le choix entre FreeCAD et OpenSCAD. Le 1er utilisant une interface utilisateur devrait donc être plus facile d'utilisation que OpenSCAD qui lui construit une structure 3D à partir de codes utilisant un langage proche de python.    
Voyant qu'une grande majorité de mes collègues ont choisi d'utilisé OpenSCAD et que python ne m'est pas inconnu j'ai décidé de suivre le mouvement pour pouvoir continuer à poser mes questions.    
Apparemment, d'après [Wékipédia](https://fr.wikipedia.org/wiki/FreeCAD), il serait possible d'intégrer un module OpenSCAD dans FreeCAD. Malheureusement je n'ai pas eu le temps d'explorer cette possibilité.

![openscad1](../images/openscad1.png)

## 2.6 Licence CC (Creative Commons)
C'est un moyen normalisé d'accorder au public l'autorisation d'utiliser leur travail créatif en vertu de la loi sur le droit d'auteur.
Grâce au [site creative commons](https://creativecommons.org/share-your-work/) j'ai plus facilement choisi ma licence :  the Creative Commons Attribution-ShareAlike 4.0 International License. 
Licence que j'utiliserai pour tous les travaux de ce cours. **ATTENTION, toujours la mettre en en-tête avec le nom de l'auteur et la date**. J'ai pris exemple sur Stanislas pour la démarche de l'en-tête. 

![Licence](../images/licence.png)

## 2.7. Première approche sur OpenSCAD
Consulte [cheat sheet](https://openscad.org/cheatsheet/) pour avoir toutes les commandes de OpenSCAD. 
### 2.7.1 Formes géométrique 
On a différente formes géométriques disponible, il est intéressant de noté que pour les courbes on a une forme de "pixélisation". 

![openscad2](../images/openscad2.png)

Cette "pixélisation" peut être corrigée par la fonctionnalité : `$fn=`(en gros on va ajouter plus de surfaces)
![openscad3](../images/openscad3.png)
![openscad4](../images/openscad4.png)

### 2.7.2 Opérations booléennes 
C'est tout l'intérêt du logiciel pour la modélisation de formes atypique. On va pouvoir **combiner des formes ensemble** 
grâce à des fonctions comme 
`union`.    
![booléen1](../images/booleen1.png)
![booléen2](../images/booleen2.png)

On peut également définir une surface couvrant tous les coins externes des formes assemblées grâce à la fonction `hull`. 
![booléen3](../images/booleen3.png)
![booléen4](../images/booleen4.png)   

On peut également faire la différence des formes avec la fonction `difference`. 
![booléen5](../images/booleen5.png)
![booléen6](../images/booleen6.png)

## 2.8. Modélisation de ma pièce Flexlinks 
- **Il est important de commencer par les paramètres** car c'est dans la partie impression 3D qu'on réajustera ceux-ci pour les combinés a des legos.       
- Ensuite le but sera de modéliser le *lego-part* en commençant par la formation de 2 cylindres qu'on relira avec la fonction `hull`(On utilisera ces cylindres comme paramètre pour construire les trous). 

- Puis On espacera les 2 "trous" d'une distance égale aux nombres de trous multipliés par l'espacement. (Les "trous" sont en faites les cylindres du début qui sont toujours plein)

- On va utiler des boucles pour faire le nombre de trous voulus. Le module *lego-part* ainsi fait nous poura être dupliquer.

- On relira les 2 "lego-part".   

 Remarque : Je n'ai pas eu le temps d'étudier ça par moi-même du coup j'ai travaillé avec Stanislas qui m'a autorisé a modélisé ça pièce :) . 

```
/*
@File    :   fixed_fixed_Beam.scad
@Time    :   2023/02/23 19:21:11
@Author  :   Stanislas Mondesir 
@Version :   1.0
@License :   Creative Commons Attribution-ShareAlike 4.0 International License.
@Desc    :   None
*/

nbHole = 2;

diametreHole = 10;
spaceBetween = 2;
border = 2;
lPHeight = 10;
rodeHeight = 5;

radius = 20;
angles = [-20, 45];
width = 2;
fn = 24;


module makesuport(){
    realSpaceBetween = spaceBetween-border;

    difference(){
        hull(){
            cylinder(d=diametreHole+border, h=lPHeight);
            translate([(diametreHole+border+realSpaceBetween)*(nbHole-1), 0,0])
                cylinder(d=diametreHole+border, h=lPHeight);
        }
        for (i=[0:nbHole-1]){
            translate([(border + diametreHole + realSpaceBetween) * i,0,-1]){
                cylinder(h=lPHeight+5, d=diametreHole);
            }
            translate([(border + diametreHole + realSpaceBetween) * i,0,lPHeight])
                sphere(d=diametreHole*1.07);
            translate([(border + diametreHole + realSpaceBetween) * i,0,0])
                sphere(d=diametreHole*1.07);
        }
    }
}




rodeWidth = border+diametreHole-border;
rectLenght = border+diametreHole*(nbTrou)+spaceBetween*(nbTrou-1);
center = border/2 + diametreHole/2;



rotate([0,0,angles[0]]) translate([center+radius-rodeWidth/2, -center+border/2, 0]) rotate([0,0,-90]) makesuport();

//src: https://openhome.cc/eGossip/OpenSCAD/SectorArc.html
module sector(radius, angles, fn = 24) {
    r = radius / cos(180 / fn);
    step = -360 / fn;

    points = concat([[0, 0]],
        [for(a = [angles[0] : step : angles[1] - 360]) 
            [r * cos(a), r * sin(a)]
        ],
        [[r * cos(angles[1]), r * sin(angles[1])]]
    );

    difference() {
        circle(radius, $fn = fn);
        polygon(points);
    }
}

module arc(radius, angles, width = 1, fn = 24) {
    difference() {
        sector(radius + width, angles, fn);
        sector(radius, angles, fn);
    }
} 

linear_extrude(rodeHeight) arc(radius, angles, width);

rotate([0,0,angles[1]-5]) translate([center+radius-rodeWidth/2, center+border/2, 0]) rotate([0,0, 90]) makesuport();

``` 
 ![lego-part](../images/lego-part.png)    






