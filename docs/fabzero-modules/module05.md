# 5. Dynamique de groupe et projet final
Le but étant de trouver une thématique et une problématique en groupe. 
## 5.1. Analyse et conception de projet 
### 5.1.1 Analyse des problèmes et des arbres d'objectifs 
En binôme avec **Stanislas**, on a décidé de trouver un **problème** qu'un projet fablab pourrait résoudre. Le projet choisi est le [DremelFuge](https://www.thingiverse.com/thing:1483), une centrifugeuse low cost.     
La problématique choisi est **la difficulté de production des outils médicaux.**     
De la problématique on en a déterminé les **causes** et **conséquences** (visible sur le 1e arbre).    
### 5.1.2 Transformation de l'arbre à problèmes en arbres à objectifs
On a reformulé la problématique en **objectifs** et remplacer les causes par le projet mis en oeuvre.      
 ![arbre](../images/arbre_temporaire.png) 
## 5.2 Formation de groupe et remue-méninges sur les problématiques du projet. 
### 5.2.1 Création du groupe et lancement de la thématique
L'objet apporté est un gobelet en PLA, il m'évoque la problématique du bioplastique. En effet, le bioplastique est une solution transitionnel du coup [une fausse solution](https://www.mdpi.com/2073-4441/14/22/3596).     
Premièrement, toute la classe a dû présenter, à tour de rôle, son objet et le pourquoi de son choix.     
Ensuite ,après cette écoute **on a dû nous regrouper par groupe de 4** en essayant d'avoir des problématiques les plus proche possible. Ayant une problématique beaucoup trop précise ce fu très complexe de trouver un groupe.     
Voici une liste des problématiques de chaque membre du groupe :


| Nom | Objet | Problématique |
| ------------------ |-----------------------| --------------------------------------------------- |
| `Donovan Crousse`| Peau de bannane | Problème écologique lié au transport et au gaspillage| 
| `Alexandre Hallemans`| baguettes | La disponibilité des matières première et de l'utilité des objets simples |
| `Louis Devroye `| Un produit médical | les tests sur les animaux |

Puis une fois regrouper on a dû écrire des mots et **des phrases pouvant mettre en lien nos problématiques respective.** Ce travail fut individuel. 
Suite à cela on a fait une mise en commun.      
Par la suite on a **chacun proposer une thématique** et, en toute démocratie, on en a choisi une pour le groupe.           

![thématique](../images/choix_de_th%C3%A9matique.jpg)      

La conclusion du choix démocratique est tout simplement que notre thématique est **le recyclage.**     
L'étape suivante est de trouver une liste de problématiques en utilisant les termes `Si j'étais toi`. Ce fut également complexe car déjà il fallait avoir de l'inspiration mais surtout **il fallait se forcer à trouver des problématiques et non des solutions**. 

![problématiques](../images/Probl%C3%A9matiques.jpg)           
Les problématiques trouvées tournaient autour du plastique.    
Ensuite il fallait, grâce à notre thématique, que les autres groupes trouvent de nouvelles problématiques pour nous. Nous avons dû faire de même pour eux. Ce qui fu intéressant car cela nous a permis de se questionner sur **les problématiques liées aux procédés de conception des bioplastiques**.

![avis'autres groupes](../images/Avis_d'autres_groupe.jpg)

### 5.2.2. Outils de dynamique de groupe 
Chloé Crokart nous a donnez un cours pour améliorer notre gestion de projet en groupe pour ainsi améliorer notre dynamique de groupe. 
#### 5.2.2.1. Méthodes de prises de décision en équipes. 
La prise de décision en équipe est plus difficile que la prise de décisions individuelles car elle impose de prendre en compte de multiples points de vue. C'est pourquoi des méthodes de prises de décisions sont nécessaire pour garder une bonne cohésion dans le groupe.     
L'une d'entre elle est **Le vote par points/multivote**. Elle consiste à ce que chaque membre de l'équipe soumet d'abord les idées sur lesquelles voter et ensuite chacun choisira la meilleur option pour eux. L'option choisit sera celui qui aura le plus de votes (vote de la majorité).      
Une autre méthode serait **La technique du groupe nominal**. Elle consiste à ce que chaque membre du groupe note toutes les solutions ou idées  qui leur viennent à l'esprit par rapport au problème traiter dans un laps de temps déterminé. Ensuite un responsable inscrit les idées et suite à un vote les idées les plus populaires seront garder (exactement ce qu'on a dû faire avec nos objets). Enfin on va chacun classer les idées par ordre de priorité et l'idée prioritaire sera gardé. Technique que je trouve très intéressante pour favoriser la créativité et pour élargir notre réflexion.   
#### 5.2.2.2. Ordre du jour 
Rédiger un ordre du jour permet d'avoir la listes des sujets qui seront abordés lors de la réunion. Ce qui améliore ainsi considérablement la communication des informations et la gestion du temps. En effet, celui-ci facilite la visualisation des priorités d'un projet.   
#### 5.2.2.3. La "météo" 
Prélevé la "météo" consiste à voir comment les membres du groupe se sentent ce qui est pratique pour prévoir les tensions au sein de celui-ci. Il est préférable de la prendre au début et à la fin de chaque séance. 
### 5.2.3. Le Lancement du projet 
Une fois les membres du groupe trouvés et accordés sur une thématique (qui est ici **le recyclage**) et munis des outils de la dynamique de groupe il est temps de se lancer dans la découverte de la problématique qui nous permettra de lancer notre projet. On déterminera cette problématique grâce à l'élaboration **d'un arbre à problèmes** et ensuite **d'un arbre à objectifs**      
![arbre a problème](../images/module5/arbre%20a%20probl%C3%A8me.jpg)       
 
Remarque : Nous avons été rejoint par un nouveau membre, `Kawtar Zaouia`. 













































