# 3. Impression 3D
## 3.1. Introduction 
Le but de ce module est d'imprimer notre pièce grâce à l'imprimante 3D. (Vu la disponibilité des imprimante j'ai travaillé avec Stanislas).      

## 3.2. PrusaSlicer 
C'est un logiciel de découpage (donc un logiciel de génération de parcours pour l'impression 3D).   
Donc a partir du fichier 3D le logiciel permettra de décomposé l'objet en plusieurs couches pour que l'imprimante puisse les imprimer couche par couche. 

![prusaslicer](../images/prucaslicer.png)    

Après l'avoir installé on le configure via le [tuto imprimantes 3D](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md).     
Il faut noter que l'imprimante utiliser sera une **Prusa I3 MK3S** ou une **Prusa I3 MK3S+**.       
![imprimante 3D](../images/Imprimante%203D.jpg)

## 3.3. Reparamétrage de la pièce
On va procéder a des tests pour limités les erreurs d'impression pour que la pièce puisse s'associer au lego via la création d'outils de mesure. 
### 3.3.1. Modélisation de la latte. 
On va tout d'abord modéliser et imprimer la latte pour voir la différence entre 1 unité openscad et 1mm dans la réalité et ainsi augmenter la précision.     

```
steps = [0.1, 1];
rulerWidth = 3;
rulerHeight = 0.1;
rulerLenght = 10;
cube([rulerWidth, rulerLenght, rulerHeight]);
for (step = steps)
    for (i = [0:step:rulerLenght])
        translate([0, i, 0])
            cube([1, 0.01 , rulerHeight+0.1 * step/2]);

txt = str(steps);
translate([rulerWidth*2/3,0,rulerHeight])
    rotate([0,0,90])
    linear_extrude(rulerHeight/10)
    text(txt, rulerWidth/3, "FreeSerif");
```
![latte](../images/latte.png)    
![latte](../images/latte2.jpg)

### 3.3.2. Modélisation des trous 
On va coder une sorte de latte a partir de notre modèle de lego. (On va jouer sur le diamètre des trous). (On va diminuer/augmenter les trous de 0.1 à chaque trous).     


```
nbTrouEachSide = 1;
BaseddiametreTrou = 10;
testStep = 0.1;
maxDiameter = BaseddiametreTrou + testStep * nbTrouEachSide;
espace = 2;
border = 2;
rectHeight = 10;

Realespace = espace-border;

module makesuport(){
    difference(){
        hull(){
            translate([(maxDiameter+border+Realespace)*-nbTrouEachSide, 0,0])
                cylinder(d=maxDiameter+border, h=rectHeight);
            translate([(maxDiameter+border+Realespace)*nbTrouEachSide, 0,0])
                cylinder(d=maxDiameter+border, h=rectHeight);
        }
        for (i=[-nbTrouEachSide:nbTrouEachSide]){
            translate([(border + maxDiameter + Realespace) * i,0,-1]){
                cylinder(h=rectHeight+5, d=BaseddiametreTrou+(i)*testStep);
            }
        }
    }
}
txt = str(str(BaseddiametreHole), ":" , str(testStep));
translate([0,-((border + maxDiameter)/2),0])
    rotate([90,0,0])
    linear_extrude(1)
    text(txt, rectHeight-1, "FreeSerif", "center", "center", halign="center");
makesuport();
```
![trou](../images/trou.png)
![trouIRL](../images/trou2.jpg)      
On a réajusté l'écart entre les trous car il y avait un décalage avec le lego.       
Le résultat étant que **le diamètre du trou = 5,1mm**.    
**L'écart idéal entre les trous = 3mm**. 
### 3.3.3. Mesure de la flexibilité 
On a imprimé plusieurs pièces d'épaisseur différentes.       
On a remarqué qu'**a 4mm il n'y a aucune flexibilité**.      
On a conclue qu'**a 1,8mm la flexibilité commence à être intéressante**.      
Par contre le temps nous manquait pour faire plus de tests dessus.     

![pièce](../images/pi%C3%A8ce.jpg)
## 3.4. Imprimer la pièce 
On exporte le gcode sur PrusaSlicer, puis sur une carte SSD pour l'utiliser sur l'imprimante 3D.   
Pendant que la pièce se synthétise, on a pu constater un problème venant de l'imprimante.      
![echec](../images/echec1.jpg)        
Le problème étant qu'un amas de PLA se serait accumulé au bout de la buse ("la pointe"). On a résolu le problème en retirant ce surplus avec une pince.    
Après la résolution du problème la pièce pu se synthétiser sans embuche.       
![pièce finie](../images/pi%C3%A8ce_fini.jpg)












































