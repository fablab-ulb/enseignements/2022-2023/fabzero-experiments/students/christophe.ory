/*
@File    :   fixed_fixed_Beam.scad
@Time    :   2023/02/23 19:21:11
@Author  :   Stanislas Mondesir 
@Version :   1.0
@License :   Creative Commons Attribution-ShareAlike 4.0 International License.
@Desc    :   None
*/

nbHole = 2;

diametreHole = 10;
spaceBetween = 2;
border = 2;
lPHeight = 10;
rodeHeight = 5;

radius = 20;
angles = [-20, 45];
width = 2;
$fn = 100;


module makesuport(){
    realSpaceBetween = spaceBetween-border;

    difference(){
        hull(){
            cylinder(d=diametreHole+border, h=lPHeight);
            translate([(diametreHole+border+realSpaceBetween)*(nbHole-1), 0,0])
                cylinder(d=diametreHole+border, h=lPHeight);
        }
        for (i=[0:nbHole-1]){
            translate([(border + diametreHole + realSpaceBetween) * i,0,-1]){
                cylinder(h=lPHeight+5, d=diametreHole);
            }
            translate([(border + diametreHole + realSpaceBetween) * i,0,lPHeight])
                sphere(d=diametreHole*1.07);
            translate([(border + diametreHole + realSpaceBetween) * i,0,0])
                sphere(d=diametreHole*1.07);
        }
    }
}




rodeWidth = border+diametreHole-border;
rectLenght = border+diametreHole*(nbTrou)+spaceBetween*(nbTrou-1);
center = border/2 + diametreHole/2;



rotate([0,0,angles[0]]) translate([center+radius-rodeWidth/2, -center+border/2, 0]) rotate([0,0,-90]) makesuport();

//src: https://openhome.cc/eGossip/OpenSCAD/SectorArc.html
module sector(radius, angles, fn = 24) {
    r = radius / cos(180 / fn);
    step = -360 / fn;

    points = concat([[0, 0]],
        [for(a = [angles[0] : step : angles[1] - 360]) 
            [r * cos(a), r * sin(a)]
        ],
        [[r * cos(angles[1]), r * sin(angles[1])]]
    );

    difference() {
        circle(radius, $fn = fn);
        polygon(points);
    }
}

module arc(radius, angles, width = 1, fn = 24) {
    difference() {
        sector(radius + width, angles, fn);
        sector(radius, angles, fn);
    }
} 

linear_extrude(rodeHeight) arc(radius, angles, width);

rotate([0,0,angles[1]-5]) translate([center+radius-rodeWidth/2, center+border/2, 0]) rotate([0,0, 90]) makesuport();
