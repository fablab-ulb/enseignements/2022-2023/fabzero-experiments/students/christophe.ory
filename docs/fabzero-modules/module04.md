# 4. Outil Fab Lab sélectionné
## 4.1 Microcontrôleur (prototypage électronique)
On va apprendre à utiliser des microcontrôleurs pour prototyper des applications électroniques en programmant. 
### 4.1.1 Définition 
C'est **un circuit intégré** (CI) ou puce électronique  sont basés sur les semi-conducteurs permettant de reproduire des fonctions plus ou moins complexe. Ces CI **rassemblent les éléments essentiels d'un ordinateur** : processeur, mémoire, unités périphériques et interfaces d'entrées et de sorties.     
Ils sont surtout utilisés dans les systèmes embarqués.         
Son avantage étant qu'il permet de diminuer le cout d'un système à base de microprocesseur. 


### 4.1.2 Arduino 
C'est la marque d'une plateforme de prototypage mais également un "langage" informatique (moins avantageux que C) plus simple de compréhension.  
On peut retrouver sur ce [lien](https://www.arduino.cc/reference/en/) toutes les commandes utiles.    
Personnellement j'ai mieux compris avec [Arduino Factory](https://arduinofactory.fr/langage-arduino-void-setup/#:~:text=Le%20void%20setup%20est%20une,dans%20le%20reste%20du%20programme.) 

### 4.1.3 Raspberry Pico 
On travaillera sur un module **YD-RP2040** qui est un équivalent du microcontrôleur Raspberry Pico. Voici l'architecture de tous les éléments du module. C'est plus intéressant de travaillé sur un Raspberry qu'un Arduino car Linux est intégré dessus et que le téléchargement de programme est plus simple que Arduino.     

![YD-RP2040](../images/microcontroleur/YD-RP2040.png) 

### 4.1.4 Software 
Pour coder les instructions de notre microcontrôleur on aura le choix entre utiliser du **Python** via **thonny** (utilisation de MicroPython qui est adapté aux microcontrôleurs) ou de codé en **Arduino** sur **Arduino IDE**.      
![Thonny](../images/thonny%20.png)   
![Arduino](../images/Arduino_IDE.png) 
J'ai décidé de travaillé via **Arduino** car le logiciel à l'air plus intuitif.


### 4.1.5 Configuration de Arduino 
Après l'installation de Arduino IDE, on va ajouter un lien dans les préférences pour pouvoir calibrer l'application.     
![parametrage](../images/Arduino_parametrage.png)    
Puis chercher dans le gestionnaire de carte le modèle de microcontrôleur à avoir.       
![validation](../images/Arduino_validation.png)    
Enfin il faudra choisir la version **Generic RP2040** dans les outils pour finir le calibrage.


### 4.1.6 Chipotage 
**ATTENTION** :    
- toujours définir les ports !!!!  
- Toujours mettre les ";" à la fin de chaque commande    
- Ne pas oublier {}     

#### 4.1.6.1 Blink
Le but étant de faire clignoter une LED.   
Remarque : explication des commande dans le code.    
```    
                
 int LED =25;
void setup() {
  // void set up permet d'assigner les valeurs des variable en entrée et en sortie de la carte. 
     pinMode(LED, OUTPUT); // pinMode(2,OUTPUT) permet d'initialiser la broche en sortie pour controler un composant. 
}

void loop() { // contient le code 
  digitalWrite(LED,HIGH); // digitalWrite permet de controler le composant, ici il l'allume.
  delay(50); // Action qui dure 50 milli seconde
  digitalWrite(LED,LOW); // Ici il l'éteint. 
  delay(50);

}            
```                 

<iframe src="../../vidéo/blink.mp4" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen loop muted></iframe>  

#### 4.1.6.2 Fade 
Le but étant d'allumer et d'éteindre progressivement un LED (grâce à la fonction analogWrite)     
```
int LED =25; 
int brightness =0; // brillance de la led
int fadeAmount =5; // la valeur pemettant la variation lumineuse (plus le nbre est grand plus la variation est importante)  
void setup() {
  pinMode(LED, OUTPUT);
}

void loop() {
  analogWrite(LED,brightness);
  brightness = brightness + fadeAmount; // modifie la luminosité pour le prochain passage dans la boucle
  if (brightness <= 0 || brightness >= 255) { 
    fadeAmount = -fadeAmount ;} // change la direction du "fade" quand la valeur dépasse 255 ou est inférieur a 0
  delay(1000);// on joue sur le temps de la boucle 
}
``` 
Remarque : la grosse led en GP23 ne clignotte pas, je suppose qu elle n'utilise pas de PWM (nécéssaire pour utiliser la fonction analogWrite)

<iframe src="../../vidéo/fade.mp4" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen loop muted></iframe>  


#### 4.1.7 Programmer LED RGB
Le but étant de programmer la led RGB pour avoir une couleur.     
on a du ajouté l'extension **NeoPixel** ensuite j'ai codé sur Arduino.      
**Remarque** : chaque étape est expliqué dans le code.       
```       
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // pour être à 16 MHz
#endif 

#define BUTTON_PIN 24 //Définir bouton d'activation (pas obliger mais lumière trop puissante)
#define PIXEL_PIN 23 // Définir le LED RGB
#define PIXEL_COUNT 1 // Nombre de LED RGB  

Adafruit_NeoPixel strip(PIXEL_COUNT, PIXEL_PIN, NEO_GRB + NEO_KHZ800); //fonction strip pour délémiter l'espace d'action. 
// le 3e argument est la au cas ou on doit combiner différent types de NeoPixel 
// donc ici le NEO_KHZ800(800KHz) et le NEO_GRB(cablé en flux binaire GRB)
// en gros sert à RIEN vu qu'on utilise qu'un led.  

boolean oldState = HIGH; // boolean pour dire qu'on est en binaire donc true ou false.
int mode =0; // Pour avoir plusieurs mode d'activation (ici 2 ON/OFF)
// int est la pour convertir une valeur en une valeur int. 

void setup() {
  pinMode(BUTTON_PIN, INPUT_PULLUP); //définition de l'entrée. 
  strip.begin(); //Initialisation du NeoPixel
  strip.show(); // Initialisation de tout les pixels en "off"
}

void loop() {
  boolean newState = digitalRead(BUTTON_PIN); //Comme en haut mais par rapport a la reponse du bouton 
  // On cmmence a OFF mais si on appuie on vient a ON. 
  
  if((newState == LOW) && (oldState == HIGH)) { // pour définir les 2 modes
    delay(20);
   newState = digitalRead(BUTTON_PIN); 
   if(newState == LOW) {
     if(++mode > 4) mode = 0; //Passer au mode suivant jusqu'a 2 revient a 0
     switch(mode) {
       case 0: colorWipe(strip.Color(0, 0, 0), 50);
       break; //strip color (en bande), Black/off
       case 1: colorWipe(strip.Color(255, 0, 0), 50);
       break; // Red
       case 2: colorWipe(strip.Color(0, 255, 0), 50);
       break; // Green 
       case 3: colorWipe(strip.Color(0, 0, 255), 50);
       break; // Blue 
       case 4: colorWipe(strip.Color(127, 127, 0), 50);
       break; // Jaune recherché
      }
    }
  } 
  oldState= newState; // on relance la boucle 
}

void colorWipe(uint32_t color, int wait) {
  for (int i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, color);
    strip.show();
    delay(wait); 
  }
}// le but de la fct est de remplire la bande d'une couleur ( en appelant strip.Color)
``` 
**Conclusion** : il suffit de jouer sur les nombres de la dernière case sans toucher au 50. Il suffit de surrpimer les autres cases si on veut juste une seule couleur.    
### 4.1.8 Capteur de température et d'humidité
#### 4.1.8.1 Chaine de mesures 
Une chaine de mesures c'est la mise en série **d'un capteur, d'un contrôleur et d'un actionneur**. 
Les actionneurs sont commandés par le contrôleur en fonction des informations reçu des capteurs.     
Le capteur va recevoir une grandeur physique en entrée et va envoyer une grandeur électrique en sortie au contrôleur.     

#### 4.1.8.2 Paramètrer le capteur de température et d'humidité.   
Tout d'abord le capteur que nous utiliserons sera le **DHT20**, voici sa [Datasheet](https://cdn.sparkfun.com/assets/8/a/1/5/0/DHT20.pdf). On remarquera que le capteur a 4 branches. 
![DHT20](../images/DHT20.png)            
Il faudra le relier au microcontrôleur via un câblage précis comme montrer sur l'image car il indique l'emplacement du SDA et du SCL.  
![schéma](../images/microcontroleur/sch%C3%A9ma.png)       
**Attention** il a fallu ajouter la bibliothèque DHT20 pour que le code fonctionne. 

#### 4.1.9 Autre capteur 
N'ayant assister qu'au premier cours sur les microcontrôleurs car conflit horaire avec le cous de réparabilité je n'ai pu avoir étudier un autre capteur. 








## 4.2 Réparabilité et impression 3D
### 4.2.1 Première approche 
On a reçu chacun une boite avec un système électrique simple. Le but étant de trouver le problème et de le réparer.     
après plusieurs teste avec mon multimètre, je vis que mon problème était l'interrupteur.   

![boite](../images/boite%20r%C3%A9parabilit%C3%A9.jpg)
![intérupteur](../images/int%C3%A9rupteur.jpg)       

Le but final étant de nous faire comprendre l'importance des étapes.     
## 4.2.2 Réparation d'une pièce cassée (valise)
Pour cette mission j'ai travaillé avec [Cédric Luppens](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/cedric.luppens/). 

### 4.2.2.1 Analyse 
On a tout d'abord analysé qu'est ce qui était défectueux. Ici c'est l'une des pièces de la poignée de valise qui empêche la mobilité de celle -ci (le bouton).    
![pièce cassée](../images/r%C3%A9parabilit%C3%A9/pi%C3%A8ce%20cass%C3%A9e.jpg)    
On va reconstruire cette pièce grâce à l'imprimante 3D.   
### 4.2.2.2 Redesign
Pour commencer on va dimensionner la pièce cassée grâce à un pied à coulisse numérique pour pouvoir, par la suite, la modéliser.    
Ensuite on a cherché à simplifier la pièce pour plus de facilité dans notre modélisation.  
#### **1er modèle**
Vu la complexité de la pièce passer par Openscad aurait été beaucoup trop complexe pour nous vu nos connaissances en codage, c'est pourquoi nous sommes passé par **Solidworks** qui est un logiciel du même style que FreeCad mais en plus complet.       
![Solidworks1](../images/r%C3%A9parabilit%C3%A9/mod%C3%A9lisation1.jpg)                               
![SOlidworks2](../images/r%C3%A9parabilit%C3%A9/mod%C3%A9lisation2.jpg)          
Pour ce modèle on a cherché a enlevé tous les arrondis pour faciliter l'impression de la pièce.  


![modèle1](../images/r%C3%A9parabilit%C3%A9/mod%C3%A8le1.jpg)        


**PROBLEMES** :      
- longueurs des "ailes" du boutons non conforme        
- diamètres des trous trop justes, le ressort ne passe pas dans les trous.        
- bouton trop gros pour passer par le trou.    

#### **2e modèle** 
On a recalibré la pièce en redimensionnant ses "ailes" et en élargissant la tailles des trous de 2mm. On a également arrondi le bouton pour permettre le passage dans le trous.     
![modèle2](../images/r%C3%A9parabilit%C3%A9/mod%C3%A8le2.jpg) 
![modèle2](../images/r%C3%A9parabilit%C3%A9/mod%C3%A8le2_face.jpg)     
Contrairement au modèle précédant on a voulu imprimer la pièce sous un autre angle (imprimé debout) pour accroitre sa solidité (vu dans le cours de réparabilité). 

**PROBLEMES** :        
- Même avec des renforts le changement d'angle d'impression à provoquer un affaissement de la pièce         
- le bouton passe dans le trou mais n'est pas assez grand pour pouvoir être utiliser        
- la pièce semble avoir glisser lors de l'impression     
   

#### **3e modèle**
On a augmenté la taille du bouton de la pièce et on est revenu à une impression couchée. 
![modèle3](../images/r%C3%A9parabilit%C3%A9/mod%C3%A8le3.jpg)
![modèle3](../images/r%C3%A9parabilit%C3%A9/mod%C3%A8le3_abim%C3%A9.jpg)       
**PROBLEMES**:        
- On peut voir sur la 2e photo qu'il y a eu un problème d'impression surement du au mouvement de la pièce lors de l'impression.     
- En entrant la pièce dans la valise, les pieds de la pièce activaient automatiquement le mécanisme sans même exercer de pression sur le bouton     
**Remarque** : on a essayé de limer les pieds sans succès.     

#### **4e modèle**  
On a réduit l'épaisseur des pieds et des ailes.    
On a également changer le remplissage pour accroitre la solidité et rajouter une jupe pour la stabilisation de la pièce lors de l'impression.   
![modèle4](../images/r%C3%A9parabilit%C3%A9/mod%C3%A8le3_papier.jpg)     
**PROBLEME** : la pièce n'actionnait plus le mécanisme.      
Pour résoudre ce problème on a rajouté un petit morceau de papier a chaque pied pour accroitre ceux-ci de 2mm ce qui a marché. 

#### **5e modèle** 
On a donc rajouter 2mm aux pieds de la pièce et on a imprimer le modèle finale (en vert pour marquer le coup). 
![modèle4](../images/r%C3%A9parabilit%C3%A9/mod%C3%A8le4.jpg)
### 4.2.2.3 Conclusion 
Toutes ces modélisations nous ont conduit à la création d'une pièce fonctionnelle et donc de redonner une nouvelle vie à cette valise. 
![tous les modèles](../images/r%C3%A9parabilit%C3%A9/Toutes%20les%20pi%C3%A8ces.jpg) 







<iframe src="../../vidéo/test_final_de_réparation.mp4" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>      